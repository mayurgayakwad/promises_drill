/*3. Create another promise. Now have it reject with a value of `Rejected Promise!` without using `setTimeout`.
Print the contents of the promise after it has been rejected by passing console.log to 
`.catch` and also use `.finally` to log messgae `Promise Settled!`.*/


let myPromise = new Promise(function (resolve, reject) {
    let value = "Rejected promise!"
    reject(value)
})

myPromise.catch(function (result ) {
    console.log(result)
}).finally(()=>{
    console.log("Promise Settled!")
})
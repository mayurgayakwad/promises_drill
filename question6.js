/* 6. Write a funtion named `wait` that accepts `time` in ms and executes the function after the given time. */

// wait  = setTimeout(function, milliseconds)

function wait(time) {
    setTimeout( function () 
    {console.log(`Executes this function after ${(time/1000) + " Seconds" }`)}
    , time)

}
wait(Number(5000))